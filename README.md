## Electron File Explorer

This project is built with Electron + React.

## Requirements / Features
- the app should list the folders and files in a source folder. The list should - be ordered by size, and it should return the size and the last modification date of each element. Also, a count of the files and the total size should be provided.
- the app should provide a CLI to choose the folder and output the result
- the app should provide an API + UI to choose the folder and show the result
- the app should be production-ready. (no need to do everything,  you could list what left to be prod-ready)
- the app should run on a unix system (i.e. ubuntu)

## Project Structure

- `src/` React project directory
- `node-src/` Node.js API functions directory
- `bin/` Node.js executable
- `main.js` Electron starting point
- `preload.js` Electron renderer
- `dist/` Packaged Electron application

## CLI Usage
Make sure you're in the project directory, using `yarn` or `npm`, run the following command
```sh
# supply directory path as an argument
npm run explore [FILE_PATH]

yarn explore ~/Downloads

yarn explore ./src
```
![Explore Screenshot](explore_screenshot.png)

## App Usage

1. Navagate to "Files" page
2. Click "Select Directory" button in the page, select a target directory then hit open
3. Voila!

![Explore Screen capture](explore_screencapture.gif)

## Available Scripts

In the project directory, you can run:

### `yarn start`

The command builds the app for production to the `build` folder then runs the electron app in the production mode.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn serve && yarn dev`

For development. Builds the react app for to the `build` folder and Serves the electron app.

### `yarn dist:linux`

It bundles this app in production mode and outputs the application file in the `dist` folder.\
Once the process completes, send the `.appImage` to an Ubuntu desktop machine.\
Update the file permission to executable `chmod u+x [FILE]`\
Run the application `./FILE`

### `yarn explore [PATH]`

See [CLI Usage](#cli-usage)


## TO-DOs

- Add proper test cases
- Add CI/CD pipeline for production
- Add linter
- (Perhaps) come up with a better name and a better logo
- Figure out a better way to ship application


## Useful Links

https://www.electron.build/cli#targetconfiguration\
https://medium.com/@kitze/%EF%B8%8F-from-react-to-an-electron-app-ready-for-production-a0468ecb1da3\
https://github.com/electron-userland/electron-builder/issues/2404
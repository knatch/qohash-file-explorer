import { Button, HTMLTable, Card, Elevation, Icon, Alignment } from '@blueprintjs/core';
import { useEffect, useState } from 'react';

function Explorer() {
  let [files, setFiles] = useState([])
  let [path, setPath] = useState('')
  let [size, setSize] = useState(0)

  function handleOnClick(e) {
    e.preventDefault()
    window.postMessage({
      type: 'SELECT_DIR'
    })
  }

  function handleRefresh(e) {
    e.preventDefault()
    setFiles([])
    setPath('')
    setSize(0)
  }

  function _messageEvent(event) {
    if (event.data.type === 'READ_DIR') {
      const filesArray = []
      const payload = event.data.payload
      setPath(payload.path)
      setSize(payload.totalSize)
      let value
      for ([, value] of Object.entries(payload.data)) {
        filesArray.push(value)
      }
      setFiles(filesArray)
    }
  }

  useEffect(() => {
    console.log('use effect triggered - registered localStorage')
    window.addEventListener('message', _messageEvent, true);

    return function cleanup() {
      window.removeEventListener('message', _messageEvent, true);
    }
  }, [])

  return (
    <section className="bp3-dark">
      <Card interactive={true} elevation={Elevation.TWO}>
        <h3 className="explorer-header">File Explorer</h3>
        <Button onClick={handleOnClick} icon="folder-close" intent="success" text="Select Directory" />
        &nbsp;&nbsp;
        <Button onClick={handleRefresh} icon="refresh" text="Reset" />
      </Card>

      <div className="explorer-container" align={Alignment.LEFT}>
        <p>Path: <b>{path}</b></p>
        <p>Total size: <b>{size === 0 ? '' : size}</b></p>
      </div>

      { (files && files.length > 0) &&
        <HTMLTable className="explorer-container" bordered={true}>
          <thead>
            <tr>
              <th>Name</th>
              <th>Size</th>
              <th>Last Modified</th>
            </tr>
          </thead>
          <tbody>
            {files.map(file => {
              return (
                <tr key={file.name.replace(' ', '_')}>
                  <td>
                    {file.type === 'Dir' ?
                      <span><Icon icon='folder-close' />&nbsp;</span>
                      : ''
                    }
                    {file.name}
                  </td>
                  <td>{file.type === 'Dir' ? '--' : file.size}</td>
                  <td>{file.modified}</td>
                </tr>
              )
            })}
          </tbody>
        </HTMLTable>
      }
    </section>
  );
}

export default Explorer
import { Alignment, Button } from '@blueprintjs/core'
import { useHistory } from 'react-router-dom'

function Home() {
  const history = useHistory()

  function navigate() {
    history.push('/files')
  }

  return (
    <>
      <h1 align={Alignment.CENTER}>Home</h1>

      <div class="home-container">

        <h2>Requirements / Features</h2>
        <ul>
          <li>the app should list the folders and files in a source folder. The list should be ordered by size, and it should return the size and the last modification date of each element. Also, a count of the files and the total size should be provided.</li>
          <li>the app should provide a CLI to choose the folder and output the result</li>
          <li>the app should provide an API + UI to choose the folder and show the result</li>
          <li>the app should be production-ready. (no need to do everything,  you could list what left to be prod-ready)</li>
          <li>the app should run on a unix system (i.e. ubuntu)</li>
        </ul>

        <h2>App Usage</h2>
        <p>
          <ol>
            <li>Navagate to <Button className="bp3-icon-document" text="Files" onClick={navigate} /> page</li>
            <li>Click <Button className="bp3-icon-document" text="Select Directory" /> button in the page, select a target directory then hit open</li>
            <li>Voila!</li>
          </ol>
        </p>

        <h2>CLI Usage</h2>
        <div>
          <p>Make sure you're in the project directory, using `yarn` or `npm`, run the following command</p>
          <pre className="bp3-monospace-text bp3-code">
            <code>
              # supply directory path as an argument<br />
              npm run explore [FILE_PATH]<br />
              <br />
              yarn explore ~/Downloads<br />
              <br />
              yarn explore ./src
            </code>
          </pre>
        </div>

      </div>
    </>
  );
}

export default Home
// import './App.css';
import { Navbar, Button, Alignment } from '@blueprintjs/core';
import { BrowserRouter as Router, Route, Link, Redirect } from 'react-router-dom';

import Explorer from './components/explorer'
import Home from './components/home'

function App() {
  return (
    <div className="App">
        <Router>

        <Navbar className="bp3-dark">
          <Navbar.Group align={Alignment.LEFT}>
            <Navbar.Heading>Qohash - File Explorer</Navbar.Heading>
            <Navbar.Divider />
            <Button className="bp3-minimal">
              <Link className="bp3-button bp3-minimal bp3-icon-home" to="/">Home</Link>
            </Button>
            <Button className="bp3-minimal">
              <Link className="bp3-button bp3-minimal bp3-icon-document" to="/files">Files</Link>
            </Button>
          </Navbar.Group>
        </Navbar>

        <section className="bp3-dark">
          <Route path="/files/" component={Explorer} />
          <Route path="/" exact component={Home} />
          <Redirect to="/" />
        </section>
      </Router>

    </div>
  );
}

export default App;

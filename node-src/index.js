const getDirStats = require('./getDirStats')
const getTotalSize = require('./getTotalSize')

module.exports = {
    getDirStats,
    getTotalSize
}
const fs = require('fs');
const path = require('path');
const { promisify } = require('util')
const { exec } = require('child_process')

const execCmd = promisify(exec)

async function getTotalSize (dir) {
    // console.log('getTotalSize: ', dir)
    
    // replace whitespace with \whitespace
    const dirPath = dir.replace(/ /g, '\\ ')
    // console.log('dir path: ', dirPath)
    try {
        const { stdout } = await execCmd(`du -sh ${dirPath}`)
        const size = stdout.replace(dir, '').trim()
        return size + 'B'
    } catch (e) {
        console.error(e)
    }
}

module.exports = getTotalSize
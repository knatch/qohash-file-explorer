const fs = require('fs')
const path = require('path')
const { promisify } = require('util')

const readDir = promisify(fs.readdir)
const readStat = promisify(fs.stat)

async function readDirFiles (dir) {
    try {
        const files = await readDir(dir)
        return Promise.all(files
            .filter(file => file.substring(0, 1) !== '.') // remove .DS_STORE
            .map(async file => {
                const filePath = path.join(dir, file)
                
                const fileStats = await readStat(filePath)
                const fileObj = {
                    name: file,
                    byteSize: fileStats.size,
                    size: fileStats.isFile() ? convertFileSize(fileStats.size) : '--',
                    type: fileStats.isDirectory() ? 'Dir' : 'File',
                    modified: fileStats.mtime.toLocaleString(),
                    path: filePath
                }
                return fileObj
            })
        )
    } catch (e) {
        console.error(e)
    }
}

function getSortedData (data) {
    return data.sort((a, b) => a.byteSize - b.byteSize)
}

function convertFileSize (size) {
    const i = Math.floor( Math.log(size) / Math.log(1024) )
    return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i]
}

async function getDirStats (dir) {
    const data = await readDirFiles(dir)
    const sortedData = getSortedData(data)
    return sortedData
}

module.exports = getDirStats
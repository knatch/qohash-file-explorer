const { app, BrowserWindow, dialog, ipcMain } = require('electron');
const { getDirStats, getTotalSize } = require('./node-src')
const path = require('path');

let win;
let mainWindow;

function createWindow() {
    win = new BrowserWindow({
        width: 900,
        height: 750,
        webPreferences: {
            nodeIntegration: false,
            preload: path.join(__dirname, 'preload.js'),
            enableRemoteModule: false,
            contextIsolation: true,
            sandbox: true
        }
    });
    
    
    if (process.env.DEBUG) {
        win.loadURL(`http://localhost:3000`);
        // win.webContents.openDevTools()
    } else {
        win.loadURL(`file://${__dirname}/build/index.html`);
    }
    
    win.on('closed', () => {
        win = null;
    })
;}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});

ipcMain.on('SELECT_DIR', async (event, arg) => {
    const result = await dialog.showOpenDialog(mainWindow, {
      properties: ['openDirectory']
    })
    // console.log('directories selected', result.filePaths)
    if (result.filePaths && result.filePaths.length) {
        const dir = result.filePaths[0]

        const data = await getDirStats(dir)
        const totalSize = await getTotalSize(dir)

        const obj = {
            path: dir,
            totalSize,
            data
        }

        const parsedData = JSON.stringify(obj)
        // console.log(data, parsedData)
        await win.webContents.executeJavaScript(`window.postMessage({type: 'READ_DIR', payload: ${parsedData}})`)
        // await win.webContents.executeJavaScript(`localStorage.setItem('filesInfo', ${whats})`)
    }
  })
const { ipcRenderer } = require('electron')

process.once('loaded', () => {
  window.addEventListener('message', evt => {
    if (evt.data.type === 'SELECT_DIR') {
      ipcRenderer.send('SELECT_DIR')
    }
  })
})
const args = process.argv
const chalk = require('chalk')
const { getDirStats, getTotalSize } = require('../node-src')
const log = console.log;

async function main() {
    // default to current directory
    let dirPath = __dirname
    if (args[2]) dirPath = args[2].toString()

    console.log('dirPath: ', dirPath)
    const data = await getDirStats(dirPath)
    const totalSize = await getTotalSize(dirPath)
    const formattedData = formatTable(data)
    const obj = {
        path: dirPath,
        totalSize,
        data: formattedData
    }

    logResult(obj)
}

function formatTable (data) {
    let str = ''
    let item

    for (item of data) {
        str += `\t${item.name}       ${item.size}\t${item.modified}\n`
    }

    return str
}

function logResult ({ path, totalSize, data }) {
    log('====================== RESULT ======================')
    log(`
    Path: ${chalk.blue.bold(path)}
    Total Size: ${chalk.blue.bold(totalSize)}
    Data: ${chalk.bold('\n\tName\t|\tSize\t|\tLast Modified')}
    ${chalk.blueBright(data)}
    `)
    log('==================== END RESULT ====================')
}

if (require.main === module) {
    main();
}